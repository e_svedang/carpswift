import PackageDescription

let package = Package(
  name: "CarpSwift",
  dependencies: [.Package(url: "https://github.com/harlanhaskins/LLVMSwift.git", majorVersion: 0)]
)
