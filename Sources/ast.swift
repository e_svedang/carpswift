import Foundation
import LLVM

let printTypeDescription = true

class AST : CustomStringConvertible {
    let scope : Scope
    let type : CarpType
    init(scope: Scope) {
        self.scope = scope
        self.type = makeUniqueTypeVar()
    }
    var description : String {
        return "<AST>"
    }
    var typeDescription : String {
        if printTypeDescription {
            return " : \(type.description)"
        } else {
            return ""
        }
    }
    func buildIR(module : Module, builder : IRBuilder) -> IRValue {
        fatalError("Must override buildIR() in subclass.")
    }
}

enum Op {
    case Add, Mul, Sub, Div
}

class ASTBinop : AST {
    let op : Op
    let lhs : AST
    let rhs : AST
    init(op: Op, lhs: AST, rhs: AST, scope: Scope) {
        self.op = op
        self.lhs = lhs
        self.rhs = rhs
        super.init(scope: scope)
    }
    override var description: String {
        return "<\(op) \(lhs) \(rhs)\(typeDescription)>"
    }
    override func buildIR(module: Module, builder: IRBuilder) -> IRValue {
        let l = lhs.buildIR(module: module, builder: builder)
        let r = rhs.buildIR(module: module, builder: builder)
        switch op {
        case .Add:
            return builder.buildAdd(l, r)
        case .Sub:
            return builder.buildSub(l, r)
        case .Mul:
            return builder.buildMul(l, r)
        case .Div:
            return builder.buildDiv(l, r)
        }
    }
}

class ASTNumber : AST {
    let number : Double
    init(number: Double, scope: Scope) {
        self.number = number
        super.init(scope: scope)
    }
    override var description: String {
        return "<\(number)\(typeDescription)>"
    }
    override func buildIR(module: Module, builder: IRBuilder) -> IRValue {
        //return FloatType.double.constant(number)
        return IntType.int32.constant(Int(number))
    }
}

class ASTString : AST {
    let string : String
    init(string: String, scope: Scope) {
        self.string = string
        super.init(scope: scope)
    }
    override var description: String {
        return "<\"\(string)\"\(typeDescription)>"
    }
//    override func buildIR(module: Module, builder: IRBuilder) -> IRValue {
//        
//    }
}

class ASTVariable : AST {
    let name : String
    init(name: String, scope: Scope) {
        self.name = name
        super.init(scope: scope)
    }
    override var description: String {
        return "<var \(name)\(typeDescription)>"
    }
    override func buildIR(module: Module, builder: IRBuilder) -> IRValue {
        
        if let owningScope = scope.resolve(variableName: name) {
            if let binder = owningScope.bindings[name] {
                if let irValue = binder.irValue {
                    if owningScope.scopeKind == .Global {
                        return builder.buildLoad(irValue)
                    } else {
                        return irValue
                    }
                } else {
                    fatalError("Missing irValue on binder for variable '\(name)'.")
                }
            } else {
                fatalError("Can't find binder '\(name)' in bindings.")
            }
        } else {
            fatalError("Can't find scope with variable '\(name)'.")
        }
        
//        if let global = module.global(named: name) {
//            return global
//        } else {
//            return Int32(0)
//            //fatalError("Can't find global variable named '\(name)'.")
//        }
    }
}

class ASTArg : AST {
    let name : String
    init(name: String, scope: Scope) {
        self.name = name
        super.init(scope: scope)
    }
    override var description: String {
        return "<arg \(name)\(typeDescription)>"
    }
//    override func buildIR(module: Module, builder: IRBuilder) -> IRValue {
//        
//    }
}

class ASTDef : AST {
    let name : String
    let expr : AST
    init(name: String, expr: AST, scope: Scope) {
        self.name = name
        self.expr = expr
        super.init(scope: scope)
    }
    override var description: String {
        return "<def \(name) \(expr)\(typeDescription)>"
    }
    override func buildIR(module: Module, builder: IRBuilder) -> IRValue {
        //return builder.addGlobalString(name: name, value: expr.description)
        var g = builder.addGlobal(name, type: IntType.int32)
        let num = expr as! ASTNumber
        g.initializer = Int32(num.number)
        if let binder = scope.bindings[name] {
            binder.irValue = g
        } else {
            fatalError("Missing binder for global variable '\(name)'.")
        }
        return g
    }
}

class ASTDefn : AST {
    let name : String
    let args : [ASTArg]
    let body : AST
    init(name: String, args: [ASTArg], body: AST, scope: Scope) {
        self.name = name
        self.args = args
        self.body = body
        super.init(scope: scope)
    }
    override var description: String {
        let argsAsString = args.map({ $0.description }).joined(separator: ", ")
        return "<defn \(name) [\(argsAsString)] \(body)\(typeDescription)>"
    }
    override func buildIR(module: Module, builder: IRBuilder) -> IRValue {
        let argTypes = args.map({ _ in IntType.int32 })
        let fType = FunctionType(argTypes: argTypes, returnType: IntType(width: 32))
        let function = builder.addFunction(name, type: fType)
        
        // Set the binders' IR-value (used when refering to the argument inside the function)
        for arg in args.enumerated() {
            if let binder = scope.bindings[arg.element.name] {
                binder.irValue = function.parameters[arg.offset]
            }
            else {
                fatalError("Function \(self.name) is missing the binding '\(arg.element.name)'")
            }
            
            // Set the name for nicer IR
            var param = function.parameters[arg.offset]
            param.name = arg.element.name
        }
        
        
        builder.positionAtEnd(of: function.appendBasicBlock(named: "entry"))
        let b = body.buildIR(module: module, builder: builder)
        builder.buildRet(b)
        return function
    }
}

class ASTDeclare : AST {
    let name : String
    let args : [ASTArg]
    init(name: String, args: [ASTArg], scope: Scope) {
        self.name = name
        self.args = args
        super.init(scope: scope)
    }
    override var description: String {
        let argsAsString = args.map({ $0.description }).joined(separator: ", ")
        return "<declare \(name) [\(argsAsString)]\(typeDescription)>"
    }
    override func buildIR(module: Module, builder: IRBuilder) -> IRValue {
        let argTypes = args.map({ _ in IntType.int32 })
        let fType = FunctionType(argTypes: argTypes, returnType: IntType(width: 32))
        let f = builder.addFunction(name, type: fType)
        return f
    }
}

class ASTCall : AST {
    let name : String
    let args : [AST]
    init(name: String, args: [AST], scope: Scope) {
        self.name = name
        self.args = args
        super.init(scope: scope)
    }
    override var description: String {
        let argsAsString = args.map({ $0.description }).joined(separator: ", ")
        return "<call \(name) \(argsAsString)\(typeDescription)>"
    }
    override func buildIR(module: Module, builder: IRBuilder) -> IRValue {
        if let f = module.function(named: name) {
            let irArgs = args.map({ $0.buildIR(module: module, builder: builder) })
            let call = builder.buildCall(f, args: irArgs)
            return call
        } else {
            fatalError("Can't call undefined function '\(name)'")
        }
    }
}

class ASTDo : AST {
    let statements : [AST]
    init(statements: [AST], scope: Scope) {
        self.statements = statements
        super.init(scope: scope)
    }
    override var description: String {
        let statmentsAsString = statements.map({ $0.description }).joined(separator: "; ")
        return "<do [\(statmentsAsString)]\(typeDescription)>"
    }
    override func buildIR(module: Module, builder: IRBuilder) -> IRValue {
        var finalStatement : IRValue? = nil
        for statement in statements {
            finalStatement = statement.buildIR(module: module, builder: builder)
        }
        if let finalStatement = finalStatement {
            return finalStatement
        } else {
            fatalError("What should an empty 'do' block return?")
        }
    }
}

class ASTIf : AST {
    override func buildIR(module: Module, builder: IRBuilder) -> IRValue {
        return Int32()
        //return builder.buildCondBr(condition: <#T##IRValue#>, then: <#T##BasicBlock#>, else: <#T##BasicBlock#>)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////

func sexprToAST(sexpr : Obj, scope: Scope) -> AST? {
    if let list = sexpr as? ObjList {
        if list.list.count == 3 &&
           list.list[0].description == "def" &&
           list.list[1] is ObjSymbol {
            if let expr = sexprToAST(sexpr: list.list[2], scope: scope) {
                let symName = list.list[1].description
                scope.bindings[symName] = Binder(name: symName, irValue: nil)
                return ASTDef(name: symName, expr: expr, scope: scope)
            }
            else {
                fatalError("Invalid expr in 'def'.")
            }
        }
        else if list.list.count == 4 &&
            list.list[0].description == "defn" &&
            list.list[1] is ObjSymbol &&
            list.list[2] is ObjList {
            if let argASTs = argsToASTs(args: list.list[2] as! ObjList, scope: scope) {
                let fnScope = Scope(parentScope: scope, scopeKind: .Function)
                if let body = sexprToAST(sexpr: list.list[3], scope: fnScope) {
                    for arg in argASTs {
                        fnScope.bindings[arg.name] = Binder(name: arg.name, irValue: nil)
                    }
                    return ASTDefn(name: list.list[1].description, args: argASTs, body: body, scope: fnScope)
                } else {
                    return nil
                }
            } else {
                fatalError("Invalid argument list for function '\(list.list[0].description)'")
            }
        }
        else if list.list.count == 3 &&
            list.list[0].description == "declare" &&
            list.list[1] is ObjSymbol &&
            list.list[2] is ObjList {
            if let argASTs = argsToASTs(args: list.list[2] as! ObjList, scope: scope) {
                return ASTDeclare(name: list.list[1].description, args: argASTs, scope: scope)
            } else {
                fatalError("Invalid argument list for declaration '\(list.list[0].description)'")
            }
        }
        else if list.list.count == 3 &&
            isBinopString(list.list[0].description) {
            if let lhs = sexprToAST(sexpr: list.list[1], scope: scope),
                let rhs = sexprToAST(sexpr: list.list[2], scope: scope) {
                let op : Op
                switch list.list[0].description {
                    case "+": op = .Add
                    case "-": op = .Sub
                    case "*": op = .Mul
                    case "/": op = .Div
                default: fatalError()
                }
                return ASTBinop(op: op, lhs: lhs, rhs: rhs, scope: scope)
            } else {
                return nil
            }
        }
        else if list.list.count > 0 && list.list[0].description == "do" {
            let statementASTs = list.list.dropFirst().map({ sexprToAST(sexpr: $0, scope: scope)! })
            return ASTDo(statements: statementASTs, scope: scope)
        }
        else if list.list.count > 0 {
            if let callFuncNamed = list.list[0] as? ObjSymbol {
                let argASTs = list.list.dropFirst().map({ (expr : Obj) -> AST in
                    if let ast = sexprToAST(sexpr: expr, scope: scope) {
                        return ast
                    } else {
                        fatalError("Failed to convert this expr to AST: \(expr)\n")
                    }
                })
                return ASTCall(name: callFuncNamed.description, args: argASTs, scope: scope)
            } else {
                fatalError("Can't call non-symbol \(list.list[0]), first class functions are not yet supported.")
            }
        }
        else {
            fatalError("Can't handle empty list yet.")
        }
    }
    else if let number = sexpr as? ObjNumber {
        return ASTNumber(number: number.number, scope: scope)
    }
    else if let string = sexpr as? ObjString {
        return ASTString(string: string.string, scope: scope)
    }
    else if let symbol = sexpr as? ObjSymbol {
        return ASTVariable(name: symbol.symbol, scope: scope)
    }
    else {
        return nil
    }
}

func argsToASTs(args: ObjList, scope: Scope) -> [ASTArg]? {
    var argASTs : [ASTArg] = []
    for o in args.list {
        if let symbol = o as? ObjSymbol {
            argASTs.append(ASTArg(name: symbol.description, scope: scope))
        } else {
            fatalError("Non-symbol found in argument list: \(o)")
        }
    }
    return argASTs
}

func isBinopString(_ s: String) -> Bool {
    return Set(["+", "-", "*", "/"]).contains(s)
}

