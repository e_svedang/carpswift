import Foundation
import LLVM

func repl(module: Module, builder: IRBuilder, scope: Scope) {
   
    let machine = try! TargetMachine() //triple: "x86_64-apple-macosx10.12.0")
    let jit = try! JIT(module: module, machine: machine)
    
    while true {
        print("λ ", terminator: "")
        let line = readLine()!
        
        if line == "q" {
            break;
        }
        
        let sexprs = read(str: line)
        
        for sexpr in sexprs {
            guard let ast = sexprToAST(sexpr: sexpr, scope: scope) else {
                fatalError("Can't create ast from s-expression: \(sexpr)")
            }
            if ast is ASTDefn || ast is ASTDef || ast is ASTDeclare {
                let ir = ast.buildIR(module: module, builder: builder)
                ir.dump()
            }
            else {
                // Build anonymous function that can be called to run the code
                
                let fType = FunctionType(argTypes: [], returnType: IntType(width: 32))
                let function = builder.addFunction("REPL_FUNCTION", type: fType)
                builder.positionAtEnd(of: function.appendBasicBlock(named: "entry"))
                let ir = ast.buildIR(module: module, builder: builder)
                builder.buildRet(ir)
                
                function.dump()
                try! module.verify()
                
                //let f = module.function(named: "REPL_FUNCTION")!
                let _ = jit.runFunction(function, args: [])
                
                //print("RESULT: " + result.asLLVM().name)
                //result.dump()
            }
        }
    }
}


