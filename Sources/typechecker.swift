import Foundation
import LLVM

indirect enum CarpType : CustomStringConvertible {
    case Variable(String)
    case Primitive(IRType)
    case Function(argTypes: [CarpType], returnType: CarpType)
    
    var description: String {
        switch self {
        case .Variable(let s):
            return "?\(s)"
        case .Function(argTypes: let args, returnType: let returnType):
            let argsTypesAsString = args.map({ $0.description }).joined(separator: ", ")
            return "((\(argsTypesAsString)) -> \(returnType.description))"
        case .Primitive(let t):
            return t.asLLVM().debugDescription
        }
    }
}

var uniqueTypeVarCounter = 0

func makeUniqueTypeVar() -> CarpType {
    let t = CarpType.Variable(String(uniqueTypeVarCounter))
    uniqueTypeVarCounter += 1
    return t
}

struct Constraint : CustomStringConvertible {
    let a : CarpType
    let b : CarpType
    
    var description: String {
        return "\(a) === \(b)"
    }
}

func generateConstraints(ast: AST) -> [Constraint] {
    var constraints : [Constraint] = []
    generateConstraintsInternal(ast: ast, constraints: &constraints)
    return constraints
}

func generateConstraintsInternal(ast: AST, constraints: inout [Constraint]) {
    switch ast {
    case let ast as ASTDefn:
        let argTypes = ast.args.map({ $0.type })
        constraints.append(Constraint(a: ast.type,
                                      b: CarpType.Function(argTypes: argTypes,
                                                           returnType: ast.body.type)))
        generateConstraintsInternal(ast: ast.body, constraints: &constraints)
        
    case let ast as ASTDo:
        constraints.append(Constraint(a: ast.type, b: ast.statements.last!.type))
        for statement in ast.statements {
            generateConstraintsInternal(ast: statement, constraints: &constraints)
        }
        
    case let ast as ASTCall:
        /*
        guard let scope = ast.scope.resolve(variableName: ast.name) else {
            fatalError("Can't resolve function '\(ast.name)'")
        }
        
        let lookupedFunc = scope.bindings[ast.name]!
        let funcType : IRType
        
        if let irValue = lookupedFunc.irValue {
            switch irValue {
            case let irFunction as Function:
                funcType = irFunction.type
            default:
                fatalError()
            }
        }
 */
        
        //constraints.append(Constraint(a: ast.type, b: ast.))
        
        for arg in ast.args {
            generateConstraintsInternal(ast: arg, constraints: &constraints)
        }
        
    case let ast as ASTBinop:
        constraints.append(Constraint(a: ast.lhs.type, b: ast.rhs.type))
        let argTypes = [ast.lhs.type, ast.rhs.type]
        constraints.append(Constraint(a: ast.type,
                                      b: CarpType.Function(argTypes: argTypes,
                                                           returnType: ast.lhs.type)))
        
    default:
        ()
    }
}

typealias Substitutions = [Int: CarpType]

func solveConstraints(constraints: [Constraint]) -> Substitutions {
    let substitutions : Substitutions = [:]
    
    return substitutions
}
