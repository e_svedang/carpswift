import LLVM
import Foundation

enum OldObj : Equatable {
    case Number(Double)
    case Symbol(String)
    case List([OldObj])
    case EndOfList
    
    public static func ==(lhs: OldObj, rhs: OldObj) -> Bool {
        switch lhs {
        case .Number(let x):
            if case .Number(let y) = rhs {
                return x == y
            } else {
                return false
            }
        case .Symbol(let s1):
            if case .Symbol(let s2) = rhs {
                return s1 == s2
            } else {
                return false
            }
        case .List(let l1):
            if case .List(let l2) = rhs {
                if l1.count != l2.count {
                    return false
                } else {
                    for i in 0 ..< l1.count {
                        if l1[i] != l2[i] {
                            return false
                        }
                    }
                    return true
                }
            } else {
                return false
            }
        case .EndOfList:
            if case .EndOfList = rhs {
                return true
            } else {
                return false
            }
        }
    }
}

func oldMain() {
    let module = Module(name: "MainModule")
    let builder = IRBuilder(module: module);
    
    let _ = builder.addGlobalString(name: "author", value: "Erik")
    
    let putcharTy = FunctionType(argTypes: [IntType(width: 32)], returnType: IntType(width: 32))
    let putchar = builder.addFunction("putchar", type: putcharTy)
    
    //let putsTy = FunctionType(argTypes: [PointerType()], returnType: IntType(width: 32))
    //let puts = builder.addFunction("puts", type: putcharTy)
    
    let fTy = FunctionType(argTypes: [], returnType: IntType(width: 32))
    let f = builder.addFunction("main", type: fTy)
    builder.positionAtEnd(of: f.appendBasicBlock(named: "entry"))
    builder.buildCall(putchar, args: [IntType.int32.constant(101)])
    builder.buildCall(putchar, args: [IntType.int32.constant(10)])
    builder.buildRet(IntType.int32.constant(0))
    
    let machine = try! TargetMachine() //triple: "x86_64-apple-macosx10.12.0")
    let jit = try! JIT(module: module, machine: machine)
    
    let _ = jit.runFunction(f, args: [])
    
    print("------------------------------------------------------------")
    module.dump()
    print("------------------------------------------------------------")
    
    //try module.emitBitCode(to: "program.bc")
    try! machine.emitToFile(module: module, type: CodegenFileType.bitCode, path: "program.bc")
    
    linkModule()
}
