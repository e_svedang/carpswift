import LLVM

//print(read(str: "aha"))
//print(read(str: "123"))
//print(read(str: "\"yo\""))
//print(read(str: "()"))
//print(read(str: "(a)"))
//print(read(str: "(a b)"))
//print(read(str: " ( a 666 c ) "))
//print(read(str: " ( (x y) (z q w) ) "))
//print(read(str: " ( (x y) \n (z q w) ) "))
//print(read(str: "a (b)"))
//print(read(str: "a (b) c (1 2 3 (4 5 6 ()))"))
//print(read(str: " ( 😀 😎 🤡 ) ")!)
//print(read(str: " ( bleh . gah oh ) ")!)

//let sexpr = read(str: "(defn foo () (+ 10 20))")!
//print(sexpr)
//print(sexprToAST(sexpr: sexpr)!)

let module = Module(name: "MainModule")
let builder = IRBuilder(module: module)
let globalScope = Scope(parentScope: nil, scopeKind: .Global)

//repl(module: module, builder: builder, scope: globalScope)

/*
compile(str:
    "(declare putchar (c))" +
    "(declare puts (s))" +
    "(def glob 120)" +
    "(defn f () (+ 97 7))" +
    "(defn g (x) (+ x 1))" +
    //"(defn h (x y) (+ x glob))" +
    "(defn main () (do (putchar (- 100 3))" +
    "                  (putchar (f))" +
    "                  (putchar (g 97))" +
    "                  (putchar glob)" +
    //"                  (puts msg)" +
    "                  (putchar 10)" +
    "                  0))",
    module: module,
    builder: builder,
    scope: globalScope)
*/

compile(str: try! String(contentsOfFile: "/Users/erik/Projects/CarpSwift/main.carp"),
        module: module,
        builder: builder,
        scope: globalScope)

print(module)

let machine = try! TargetMachine()

/*
print("\n--- Running main as JIT ---")
let jit = try! JIT(module: module, machine: machine)
let mainFn = module.function(named: "main")!
let retVal = jit.runFunctionAsMain(mainFn, args: [])
print("\n--- Return value: \(retVal) ---\n")
*/

try machine.emitToFile(module: module, type: CodegenFileType.bitCode, path: "./main.bc")
linkModule()

/*  TODO
    Riktig felhantering i t.ex. buildIR
    Definiera standarbibliotek i Swift? Eller måste det vara C-kod som länkas in?
*/
