import Foundation
import LLVM

func compile(str: String, module: Module, builder: IRBuilder, scope: Scope) {
    
    let sexprs = read(str: str)
    
    for sexpr in sexprs {
        guard let ast = sexprToAST(sexpr: sexpr, scope: scope) else {
            fatalError("Can't create ast from s-expression: \(sexpr)")
        }
        
        print(ast)
        
        let constrs = generateConstraints(ast: ast)
        print("Constraints: \(constrs)")
        print("Scope: \(scope)")
        
        let _ = ast.buildIR(module: module, builder: builder)
    }
    print("")
    
    do {
        try module.verify()
    }
    catch ModuleError.didNotPassVerification(let s) {
        module.dump()
        fatalError(s)
    }
    catch {
        fatalError("Module verification failed.")
    }
}
