import Foundation

func linkModule() {
    let task = Process()
    task.launchPath = "/usr/bin/clang"
    task.arguments = ["main.bc"]
    
    let pipe = Pipe()
    task.standardOutput = pipe
    task.launch()
}
