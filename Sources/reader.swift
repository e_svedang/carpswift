import Foundation

class Obj : CustomStringConvertible {
    var description: String {
        fatalError()
    }
}

class ObjSymbol : Obj {
    let symbol : String
    init(_ symbol : String) {
        self.symbol = symbol
    }
    override var description: String {
        return symbol
    }
}

class ObjList : Obj {
    let list : [Obj]
    init(_ list : [Obj]) {
        self.list = list
    }
    override var description: String {
        var s = "("
        for o in list.enumerated() {
            s += o.element.description
            if o.offset != list.count - 1 {
                s += " "
            }
        }
        s += ")"
        return s
    }
}

class ObjEndOfList : Obj {
    
}

class ObjNumber : Obj {
    let number : Double
    init(_ number : Double) {
        self.number = number
    }
    override var description: String {
        return String(number)
    }
}

class ObjString : Obj {
    let string : String
    init(_ string : String) {
        self.string = string
    }
    override var description: String {
        return "\"\(string)\""
    }
}

func read(str : String) -> [Obj] {
    let chars = Array(str.unicodeScalars)
    var pos = 0
    var objs : [Obj] = []
    while pos < chars.count {
        if let o = readInternal(chars: chars, pos: &pos) {
            objs.append(o)
        }
    }
    return objs
}

let letters = CharacterSet.letters
let numbers = CharacterSet(charactersIn: "0123456789")
let okInSymbol = Set([UnicodeScalar("+"),
                      UnicodeScalar("-"),
                      UnicodeScalar("*"),
                      UnicodeScalar("/")])

private func readInternal(chars: [UnicodeScalar], pos: inout Int) -> Obj? {
    while true {
        if pos >= chars.count {
            return nil
        }
        let c : UnicodeScalar = chars[pos]
        if c == " " || c == "\n" || c == "\t" {
            pos += 1
            continue
        }
        switch c {
        case "(":
            pos += 1
            if pos >= chars.count {
                fatalError("Missing ')'")
            }
            var elements : [Obj] = []
            while true {                
                if let o = readInternal(chars: chars, pos: &pos) {
                    switch o {
                    case is ObjEndOfList:
                        return ObjList(elements)
                    default:
                        elements.append(o)
                    }
                } else {
                    fatalError("Illegal object in list, read up until: (\(elements) ...)")
                }
            }
        case ")":
            pos += 1
            return ObjEndOfList()
        case "\"":
            // Read string
            var str = ""
            pos += 1
            if pos >= chars.count {
                break
            }
            while true {
                let innerChar : UnicodeScalar = chars[pos]
                if innerChar != "\"" {
                    str += String(innerChar)
                    pos += 1
                    if pos >= chars.count {
                        break
                    }
                }
                else {
                    pos += 1
                    break
                }
            }
            return ObjString(str)
        default:
            if letters.contains(c) || okInSymbol.contains(c) {
                // Read symbol
                // print("\n")
                var str = ""
                while true {
                    //print("reading symbol '\(str)', char = \(c), chars = \(chars)")
                    let innerChar : UnicodeScalar = chars[pos]
                    if letters.contains(innerChar) || okInSymbol.contains(innerChar) {
                        str += String(innerChar)
                        pos += 1
                        if pos >= chars.count {
                            break
                        }
                    }
                    else {
                        //pos += 1
                        break
                    }
                }
                return ObjSymbol(str)
            }
            else if numbers.contains(c) {
                // Read number
                var numStr = ""
                while true {
                    let innerChar : UnicodeScalar = chars[pos]
                    if numbers.contains(innerChar) {
                        numStr += String(innerChar)
                        pos += 1
                        if pos >= chars.count {
                            break
                        }
                    }
                    else {
                        break
                    }
                }
                if let num = Double(numStr) {
                    return ObjNumber(num)
                } else {
                    return nil
                }
            }
            else {
                fatalError("Can't read character: '\(chars[pos])'")
            }
        }
    }
}
