import Foundation
import LLVM

class Binder : CustomStringConvertible {
    let name : String
    var irValue : IRValue?
    init(name: String, irValue: IRValue?) {
        self.name = name
        self.irValue = irValue
    }
    var description: String {
        let s : String
        if let irValue = self.irValue {
            s = irValue.asLLVM().debugDescription
        } else {
            s = "nil"
        }
        return "{name: \(name), irValue: \(s)}"
    }
    
}

enum ScopeKind {
    case Global
    case Function
}

class Scope : CustomStringConvertible {
    let parentScope : Scope?
    let scopeKind : ScopeKind
    var bindings : [String:Binder] = [:]
    init(parentScope: Scope?, scopeKind: ScopeKind) {
        self.parentScope = parentScope
        self.scopeKind = scopeKind
    }
    var description: String {
        if let p = parentScope {
            return "\(bindings.description) ----> \(p.description)"
        } else {
            return "\(bindings.description) ----> nil"
        }
    }
    func resolve(variableName: String) -> Scope? {
        if let _ = bindings[variableName] {
            return self
        } else {
            if let p = parentScope {
                return p.resolve(variableName: variableName)
            } else {
                return nil
            }
        }
    }
}
